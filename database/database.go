package database

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

func CreateConn() *sql.DB {
	//load .env file

	err := godotenv.Load()
	if err != nil {
		fmt.Println("error reading .env files")
	}

	uname := os.Getenv("DB_USERNAME")
	pass := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	conf := fmt.Sprintf("%s:%s@tcp(127.0.0.1:3306)/%s", uname, pass, dbname)

	db, err := sql.Open("mysql", conf)
	if err != nil {
		defer CloseConn(db)
		panic(err.Error())
	}

	return db
}

func CloseConn(db *sql.DB) {
	db.Close()
}
