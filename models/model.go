package models

import (
	"database/sql"

	"github.com/ranggaadi/gin-gonic-learn/database"
)

type Student struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Grade int    `json:"grade"`
}

func (s *Student) GetAll() []Student {
	db := database.CreateConn()
	defer database.CloseConn(db)

	rows, err := db.Query("SELECT * FROM students")
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()

	var results []Student

	for rows.Next() {
		var each Student

		var err = rows.Scan(&each.ID, &each.Name, &each.Age, &each.Grade)
		if err != nil {
			panic(err.Error())
		}

		results = append(results, each)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	return results
}

func (s *Student) GetSingle(id int) (Student, error) {
	db := database.CreateConn()
	defer database.CloseConn(db)

	result := Student{}

	err := db.QueryRow("select * from students where id = ?", id).
		Scan(&result.ID, &result.Name, &result.Age, &result.Grade)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (s *Student) Add() (sql.Result, error) {
	db := database.CreateConn()
	defer database.CloseConn(db)
	//[BUG] kurang validasi pada kolom json

	name := s.Name
	age := s.Age
	grade := s.Grade

	res, err := db.Exec("INSERT INTO students (name, age, grade) VALUES (?, ?, ?)", name, age, grade)
	if err != nil {
		return nil, err
		// panic(err.Error())
	}

	return res, nil
}

func (s *Student) Update(id int) error {
	db := database.CreateConn()
	defer database.CloseConn(db)

	name := s.Name
	age := s.Age
	grade := s.Grade

	_, err := db.Exec("UPDATE students SET name = ?, age = ?, grade = ? where id = ?", name, age, grade, id)
	if err != nil {
		return err
	}

	return nil
}

func (s *Student) Delete(id int) error {
	db := database.CreateConn()
	defer database.CloseConn(db)

	_, err := db.Exec("DELETE FROM students where id = ?", id)
	if err != nil {
		return err
	}

	return nil
}
