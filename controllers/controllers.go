package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/ranggaadi/gin-gonic-learn/models"
)

var modelStudent models.Student

func GetAllStudents() gin.HandlerFunc {

	return func(c *gin.Context) {
		result := modelStudent.GetAll()
		c.JSON(http.StatusOK, result)
	}
}

func GetStudent() gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "please provide correct ID format",
			})
			return
		}

		result, err := modelStudent.GetSingle(id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "There is no student having that ID",
			})
			return
		}

		c.JSON(http.StatusOK, result)
	}
}

func AddStudent() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.BindJSON(&modelStudent)
		res, err := modelStudent.Add()

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "Please provide valid data",
			})
			return
		}

		id, err := res.LastInsertId()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "Something wrong has occured, try again later",
			})
			return
		}

		modelStudent.ID = int(id)
		c.JSON(http.StatusOK, modelStudent)
	}
}

func UpdateStudent() gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "please provide correct ID format",
			})
			return
		}

		c.BindJSON(&modelStudent)
		err = modelStudent.Update(id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "There is no student having that ID",
			})
			return
		}
		modelStudent.ID = id
		c.JSON(http.StatusOK, modelStudent)
	}
}

func DeleteStudent() gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "please provide correct ID format",
			})
			return
		}
		err = modelStudent.Delete(id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "please provide correct ID format",
			})
			return
		}
		msg := fmt.Sprintf("student with id %d is successfuly deleted", id)
		c.JSON(http.StatusOK, gin.H{
			"message": msg,
		})
	}
}
