package main

import (
	"github.com/gin-gonic/gin"
	"github.com/ranggaadi/gin-gonic-learn/controllers"
)

func main() {

	route := gin.Default()

	rv1 := route.Group("/api/v1")
	{
		student := rv1.Group("/students")
		{
			student.GET("/", controllers.GetAllStudents())
			student.POST("/", controllers.AddStudent())
			student.GET("/:id", controllers.GetStudent())
			student.PUT("/:id", controllers.UpdateStudent())
			student.DELETE("/:id", controllers.DeleteStudent())
		}
	}

	route.Run()
}
